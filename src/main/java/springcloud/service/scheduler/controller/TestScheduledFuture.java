package springcloud.service.scheduler.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import javax.annotation.Resource;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.PeriodicTrigger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: liudong
 * @Date: 18-9-21 16:16
 * @Description:
 */
@RestController
public class TestScheduledFuture {

    @Resource
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;
    private ConcurrentTaskScheduler concurrentTaskScheduler = new ConcurrentTaskScheduler(Executors.newScheduledThreadPool(64));
    //定时任务
    private ScheduledFuture<?> future;

    @RequestMapping(value = "/startCron")
    public void startCron(){
        //new PeriodicTrigger(runnableTask.getPeriodicTrigger()) 表达式
        future = threadPoolTaskScheduler.schedule(new Myrunnable(),new PeriodicTrigger(10000L));//10s执行一次
        System.out.println("定时任务开始执行");
    }

    @RequestMapping(value = "/stopCron")
    public String stopCron(){
        ScheduledThreadPoolExecutor executor = (ScheduledThreadPoolExecutor) threadPoolTaskScheduler.getScheduledExecutor();
        //获取执行线程
        BlockingQueue<Runnable> runnables = executor.getQueue();
        for (Runnable runnable : runnables) {
            ScheduledFuture<?> scheduledFuture = (ScheduledFuture<?>) runnable;//线程转定时器
            if (scheduledFuture.isCancelled()) {
                break;
            }
            System.out.println(scheduledFuture);
        }
        if (null != future){
            future.cancel(true);
        }
        System.out.println("定时任务已关闭");
        return "stopCron";
    }

    @RequestMapping(value = "/startConcurrent")
    public void startConcurrent(){
        //获取定时任务执行器
        ScheduledThreadPoolExecutor executor = (ScheduledThreadPoolExecutor) concurrentTaskScheduler.getConcurrentExecutor();
        //获取执行线程
        BlockingQueue<Runnable> runnables = executor.getQueue();
        for (Runnable runnable : runnables) {
            ScheduledFuture<?> scheduledFuture = (ScheduledFuture<?>) runnable;//线程转定时器
            if (!scheduledFuture.isCancelled()) {
                break;
            }
            System.out.println(scheduledFuture);
        }
        future = concurrentTaskScheduler.schedule(new Myrunnable(),new PeriodicTrigger(10000L));
        System.out.println("定时任务开始执行");
    }

    @RequestMapping(value = "/stopConcurrent")
    public void stopConcurrent(){
        if (null != future){
            future.cancel(true);
        }
        System.out.println("定时任务开始执行");
    }
}
class Myrunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("*******************定时任务开始执行TestScheduledFuture "+new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date()) +"*********************");
    }
}
