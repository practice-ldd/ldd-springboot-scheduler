package springcloud.service.scheduler.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Auther: liudong
 * @Date: 18-9-21 11:55
 * @Description:
 */
@Component("schedulerController")
//@ImportResource(locations = {"classpath:spring.xml"})
public class SchedulerController {
    /**
     * 定时任务，注解类型
     */
//    @Scheduled(fixedRate = 5000)//5s执行一次
    public void test(){
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-dd-MM HH:mm:ss");
        System.out.println("执行时间*******:"+ sdf.format(new Date()) + "*******定时任务执行");
    }

    public void testXml(){
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-dd-MM HH:mm:ss");
        System.out.println("执行时间*******:"+ sdf.format(new Date()) + "*******定时任务执行XML");
    }

}
