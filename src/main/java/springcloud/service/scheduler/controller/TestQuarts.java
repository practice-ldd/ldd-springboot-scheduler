package springcloud.service.scheduler.controller;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

import java.util.Date;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Component;

/**
 * @Auther: liudong
 * @Date: 18-9-21 18:15
 * @Description:
 */
public class TestQuarts {
    private static SchedulerFactory schedulerFactory = new StdSchedulerFactory();

    public void test(){
        System.out.println("***********");
    }

    public static void main(String[] args) {
        try {

//            Scheduler scheduler = schedulerFactory.getScheduler();
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            //定义一个Trigger
//            Trigger trigger = new Trigger().withIdentity("trigger1", "group1") //定义name/group
//                    .startNow()//一旦加入scheduler，立即生效
//                    .withSchedule(simpleSchedule() //使用SimpleTrigger
//                            .withIntervalInSeconds(1) //每隔一秒执行一次
//                            .repeatForever()) //一直执行，奔腾到老不停歇
//                    .build();
//            //定义一个JobDetail
//            JobDetail job = new Job(HelloQuartz.class) //定义Job类为HelloQuartz类，这是真正的执行逻辑所在
//                    .withIdentity("job1", "group1") //定义name/group
//                    .usingJobData("name", "quartz") //定义属性
//                    .build();

            //加入这个调度
//            scheduler.scheduleJob(job, trigger);

            //启动之
            scheduler.start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}

class HelloQuartz implements Job{
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDetail detail = context.getJobDetail();
        String name = detail.getJobDataMap().getString("name");
        System.out.println("say hello to " + name + " at " + new Date());
    }
}
