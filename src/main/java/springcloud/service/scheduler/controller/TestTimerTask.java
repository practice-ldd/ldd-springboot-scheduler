package springcloud.service.scheduler.controller;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Auther: liudong
 * @Date: 18-9-21 12:11
 * @Description:
 */
@Controller
@RequestMapping("/test")
public class TestTimerTask {
    private static org.apache.commons.logging.Log logger = org.apache.commons.logging.LogFactory.getLog(TestTimerTask.class);

    /**
     * (1)Timer.schedule(TimerTask task,Date time)安排在制定的时间执行指定的任务。
     * (2)Timer.schedule(TimerTask task,Date firstTime ,long period)安排指定的任务在指定的时间开始进行重复的固定延迟执行．
     * (3)Timer.schedule(TimerTask task,long delay)安排在指定延迟后执行指定的任务．
     * (4)Timer.schedule(TimerTask task,long delay,long period)安排指定的任务从指定的延迟后开始进行重复的固定延迟执行．
     * (5)Timer.scheduleAtFixedRate(TimerTask task,Date firstTime,long period)安排指定的任务在指定的时间开始进行重复的固定速率执行．
     * (6)Timer.scheduleAtFixedRate(TimerTask task,long delay,long period)安排指定的任务在指定的延迟后开始进行重复的固定速率执行．
     *
     * schedule方法：“fixed-delay”；如果第一次执行时间被delay了，随后的执行时间按 照 上一次 实际执行完成的时间点 进行计算
     * scheduleAtFixedRate方法：“fixed-rate”；如果第一次执行时间被delay了，随后的执行时间按照 上一次开始的 时间点 进行计算，并且为了”catch up”会多次执行任务,TimerTask中的执行体需要考虑同步
     */
    @RequestMapping("main")
    public String maintest() {
        Timer timer = new Timer();

        timer.schedule(new Mytask1(),500);//指定时间后执行的定时器
        timer.schedule(new Mytask2(), 2000, 1000);//指定时间(2秒)后执行,执行间隔(1s)
        timer.schedule(new Mytask3(), new Date(), 1000 * 60);
        return "main";
    }
}

class Mytask1 extends TimerTask {
    private static org.apache.commons.logging.Log logger = org.apache.commons.logging.LogFactory.getLog(Mytask1.class);
    public void run(){
        System.out.println("5秒之后执行的定时器");
    }
}

class Mytask2 extends TimerTask{
    private static org.apache.commons.logging.Log logger = org.apache.commons.logging.LogFactory.getLog(Mytask2.class);
    public void run(){
        System.out.println("每秒执行的定时器");
    }
}

class Mytask3 extends TimerTask{
    private static org.apache.commons.logging.Log logger = org.apache.commons.logging.LogFactory.getLog(Mytask3.class);
    public void run() {
        System.out.println("从某日起每分钟执行的定时器 !");
    }
}