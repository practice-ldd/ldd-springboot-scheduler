package springcloud.service.scheduler.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * @Auther: liudong
 * @Date: 18-9-21 16:28
 * @Description:
 */
@Configuration
//@ImportResource(locations = {"classpath:spring-quartz.xml", "classpath:spring.xml"})
public class beanConfig {

    @Bean("threadPoolTaskScheduler")
    public ThreadPoolTaskScheduler getThreadPoolTaskScheduler(){
        return new ThreadPoolTaskScheduler();
    }
}
